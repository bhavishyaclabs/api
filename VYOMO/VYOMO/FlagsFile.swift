//
//  FlagsFile.swift
//  VYOMO
//
//  Created by apple on 23/03/15.
//  Copyright (c) 2015 Bhavishya. All rights reserved.
//

import Foundation
import UIKit
//var responses = ViewController()
var jsonResult : NSDictionary = NSDictionary()
//var viewController = ViewController()
func get(url : String, viewController:ViewController) -> NSDictionary {
    var request : NSMutableURLRequest = NSMutableURLRequest()
    request.URL = NSURL(string: url)
    request.HTTPMethod = "GET"
    
    NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue(), completionHandler:{ (response:NSURLResponse!, data: NSData!, error: NSError!) -> Void in
        var error: AutoreleasingUnsafeMutablePointer<NSError?> = nil
        let json: NSDictionary! = NSJSONSerialization.JSONObjectWithData(data, options:NSJSONReadingOptions.MutableContainers, error: error) as? NSDictionary
          dispatch_async(dispatch_get_main_queue()){
        if (json != nil) {
            jsonResult = json
           // println(jsonResult) // not null...
            responseStatus(viewController)
        }
        }
    })

    return jsonResult
}

func post( url : String, postParams: String, viewController:ViewController) -> NSDictionary {
    var request : NSMutableURLRequest = NSMutableURLRequest()
    request.URL = NSURL(string: url)
    request.HTTPMethod = "POST"
    var bodyData = postParams
    request.HTTPBody = bodyData.dataUsingEncoding(NSUTF8StringEncoding)
    
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue(), completionHandler:{ (response:NSURLResponse!, data: NSData!, error: NSError!) -> Void in
            var error: AutoreleasingUnsafeMutablePointer<NSError?> = nil
            let json: NSDictionary! = NSJSONSerialization.JSONObjectWithData(data, options:NSJSONReadingOptions.MutableContainers, error: error) as? NSDictionary
            
            if (json != nil) {
                jsonResult = json
               // println(jsonResult)
                responseStatus(viewController)
            }
        })
    
    return jsonResult
}


func responseStatus(viewController:ViewController) {
   println(jsonResult) //not null...
    if let flag: Int = jsonResult["status"] as? Int {
        switch flag {
        case 200:
            println("Action Complete")
            //gives an alert message...
            let alert = UIAlertController(title: "Alert", message: "Action complete.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            viewController.presentViewController(alert, animated: true, completion: nil)
            
        case 100:
            //gives an alert message...
            var alert = UIAlertController(title: "Alert", message: "PARAMETER_MISSING", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            viewController.presentViewController(alert, animated: true, completion: nil)
            
        case 201,404,400:
            //gives an alert message...
            var alert = UIAlertController(title: "Alert", message: "ERROR", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            viewController.presentViewController(alert, animated: true, completion: nil)
            
        case 204,101,102,103,104,105,106,107:
            //gives an alert message...
            var alert = UIAlertController(title: "Alert", message: "INVALID", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            viewController.presentViewController(alert, animated: true, completion: nil)
            
        default:
            //gives an alert message...
            var alert = UIAlertController(title: "Alert", message: "ERROR", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            viewController.presentViewController(alert, animated: true, completion: nil)
        }
    }
}

    